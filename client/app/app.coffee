'use strict'

jQuery ($)->

  sampleItems = [
    {
      title: 'Device textG'
      imageUrl: '/assets/images/phone1.png'
      subtitle: 'Crazy Deals'
      price: 'AED 499.00'
    }
    {
      title: 'Device textD'
      imageUrl: '/assets/images/phone2.png'
      subtitle: 'Souq'
      price: 'AED 1999.00'
    }
    {
      title: 'Device textW'
      imageUrl: '/assets/images/phone3.png'
      subtitle: 'Crazy Deals'
      price: 'AED 1098.99'
    }
    {
      title: 'Device textH'
      imageUrl: '/assets/images/phone4.png'
      subtitle: 'Souq'
      price: 'AED 1099.00'
    }
    {
      title: 'Device textL'
      imageUrl: '/assets/images/phone5.png'
      price: 'AED 299.00'
    }
    {
      title: 'Device textI'
      imageUrl: '/assets/images/phone6.png'
      subtitle: 'Crazy Deals'
      price: 'AED 249.00'
    }
  ]

  $('#container1').initMyWidget {
    items: sampleItems
  }

  $('#container2').initMyWidget {
    items: sampleItems
    arrowsOnSelection: on
  }

  $('#container3').initMyWidget {
    itemWidth: 200
    itemHeight: 300
    items: sampleItems
  }

  $('#container4').initMyWidget {
    isMobile: on
    items: sampleItems
  }


  $('#container5').initMyWidget {
    backgroundColor: '#3e3e3e';
    items: sampleItems
  }
