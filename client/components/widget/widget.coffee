$.fn.initMyWidget = (options)->

  opts = $.extend {}, {
    isMobile: no
    itemWidth: 300
    itemHeight: 450
    backgroundColor: 'white'
    items: []
    slidesToShow: 4
    arrowsOnSelection: off
  }, options

#  opts.slidesToShow = Math.min 4, opts.items?.length
  if opts.isMobile
    opts.slidesToShow = 1

  opts.slickOptions = JSON.stringify {
    slidesToScroll: 1
    slidesToShow: opts.slidesToShow
  }

  opts.items = opts.items.map (item)->
    item.style = "background-image: url(#{item.imageUrl})"
    item


  console.log 'init widget', opts, $(this)


  $slickDots = $('<div/>').addClass('slick-dots')

  $(this).append HAML['widget_template'] opts


  $(this).css({
    'backgroundColor': opts.backgroundColor
    'width': "#{opts.slidesToShow * opts.itemWidth}px"
    'height': "#{opts.itemHeight}px"
  })

  $carousel = $(this).find('.carousel')

  $carousel.slick({
#    autoplay: on
    centerMode: off
    centerPadding: '0px'
    arrows: opts.isMobile
    dots: on
  })

  $(this).find('.slide-item').css {
    width: "#{opts.itemWidth}px"
    height: "#{opts.itemHeight}px"
  }

  if opts.arrowsOnSelection
    $(this).find('.carousel').addClass('selection-allowed')
    that = @
    t = null
    showArrows = (e)->
      e.stopPropagation()
      if t
        clearTimeout(t)
        t = null
      else
        $carousel.slick('slickSetOption', 'arrows', true, true)
    hideArrows = (e)->
      e.stopPropagation()
      t = setTimeout ->
        $carousel.slick('slickSetOption', 'arrows', false, true)
        t = null
      , 1000
    $(this)
      .on 'mouseover', '.carousel', showArrows
      .on 'mouseout', '.carousel', hideArrows


